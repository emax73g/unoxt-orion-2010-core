
# PlanAhead Launch Script for Post-Synthesis pin planning, created by Project Navigator

create_project -name Orion2010 -dir "D:/Xilinx/share/unoxt.orion2010/ise/planAhead_run_1" -part xc6slx25ftg256-2
set_property design_mode GateLvl [get_property srcset [current_run -impl]]
set_property edif_top_file "D:/Xilinx/share/unoxt.orion2010/ise/unoxt_top.ngc" [ get_property srcset [ current_run ] ]
add_files -norecurse { {D:/Xilinx/share/unoxt.orion2010/ise} {../src/cores/rombios} {../src/cores/clock} }
add_files [list {../src/cores/rombios/rombios.ncf}] -fileset [get_property constrset [current_run]]
set_param project.pinAheadLayout  yes
set_property target_constrs_file "unoxt_top.ucf" [current_fileset -constrset]
add_files [list {unoxt_top.ucf}] -fileset [get_property constrset [current_run]]
link_design
